# Tracking Web Server

This is a sample Flask application that implements two routes: `/ping` and `/img`.

## Requirements
- Flask==2.2.2

## Installation
1. Clone the repository


```
cd existing_repo
git remote add origin https://gitlab.com/anirudhcenationuniverse/tracking-web-server.git
git branch -M main
git push -uf origin main
```

2. Install the dependencies

pip install -r requirements.txt


## Usage
To run the application, execute the following command in the root directory of the project:

python run.py

The application will run on `http://localhost:5000/` by default.

## Routes
- `/ping`: Returns a response code 200 and string "OK" if the file "/tmp/ok" is present, otherwise returns 503 service unavailable.
- `/img`: Returns a 1x1 gif image and logs the request.

## Tests
To run the unit tests, execute the following command in the root directory of the project:

python -m unittest tests.test_app


## Todo Improvements: Concurrency and Scalability

1. Use a production-ready web server: By default, Flask uses a development server that is not designed for production use. To serve the application to many concurrent users, you can use a production-ready web server like Gunicorn or uWSGI, which can handle multiple requests and provide better performance.

2. Use a caching system: Adding a caching system, such as memcached or Redis, can help to reduce the load on the application by caching frequently-used data, such as the responses of the /ping route.

3. Use a load balancer: To distribute the load across multiple servers, you can use a load balancer, such as HAProxy or Nginx. The load balancer can route requests to multiple instances of the application, which can run on separate servers or in separate containers.


