import os
import logging
from app import app
from flask import request
from flask import Response

logger = logging.getLogger(__name__)

@app.route("/ping")
def ping():
    """
    Endpoint for the '/ping' route.
    Returns 'OK' with a 200 response code if the file '/tmp/ok' is present,
    otherwise returns 'Service Unavailable' with a 503 response code.
    """
    if os.path.exists("/tmp/ok"):
        return "OK", 200
    else:
        return "Service Unavailable", 503

@app.route("/img")
def img():
    """
    Endpoint for the '/img' route.
    Returns a 1x1 gif image and logs information about the incoming request.
    """
    log_request(request)
    return Response(b"GIF89a\x01\x00\x01\x00\x00\xff\x00,\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02\x02D\x01\x00;", content_type="image/gif")

def log_request(request):
    """
    Logs information about the incoming request.
    This information includes the request method, the request path,
    the remote address, and the user agent.
    """
    logger.info(f"Request URL: {request.url}")
    logger.info(f"Request Method: {request.method}")
    logger.info(f"Request User-Agent: {request.user_agent}")
    logger.info(f"Request IP: {request.remote_addr}")
