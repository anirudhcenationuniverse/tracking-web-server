import unittest
from unittest.mock import patch, mock_open
from flask import request
from app import app
from app.routes import log_request

class PingEndpointTestCase(unittest.TestCase):
    @patch('builtins.open', mock_open(read_data='OK'))
    def test_ping_with_file(self):
        with app.test_client() as client:
            response = client.get('/ping')
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.data, b'OK')

    @patch('builtins.open', side_effect=FileNotFoundError)
    def test_ping_without_file(self, mock_open):
        with app.test_client() as client:
            response = client.get('/ping')
            self.assertEqual(response.status_code, 503)

class ImgEndpointTestCase(unittest.TestCase):
    def test_img(self):
        with app.test_client() as client:
            response = client.get('/img')
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.content_type, 'image/gif')
            self.assertEqual(len(response.data), 35)
